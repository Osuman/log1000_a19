#include "voiture.h"

Voiture::Voiture(char id) {
    this->id = id;
}

void Voiture::ajouterMoteur(Moteur* un_moteur) {
    this->le_moteur = un_moteur;
}

void Voiture::enleverMoteur() {
    this->le_moteur = 0;
}

Moteur* Voiture::getMoteur() {
    return this->le_moteur;
}

char Voiture::getID() {
    return this->id;
}



